import random
 
def simpleAI(player,units):
	coin = random.randint(0,1)
	leng = len(units)
	randomunit = random.randint(0,leng-1)
	if(coin==1):
		#aqui la IA busca ponerse compartiendo una posición Y del jugador
		print("obtener X: "+str(player[0]))
		print(str(player[0])+" "+str(units[randomunit][1]))
		tentativeresult = [randomunit,[player[0],units[randomunit][1]]]
		
		#Esto presenta bugs ya que dos piezas pueden intentar obtener el mismo lugar, pero eso se puede corregir
		return tentativeresult
	else:
		#aqui la IA busca ponerse compartiendo una posición X del jugador
		print("obtener Y: "+str(player[1]))
		print(str(units[randomunit][0])+" "+str(player[1]))
		tentativeresult = [randomunit,[units[randomunit][0],player[1]]]
		
		#Esto presenta bugs ya que dos piezas pueden intentar obtener el mismo lugar, pero eso se puede corregir
		return tentativeresult
		
