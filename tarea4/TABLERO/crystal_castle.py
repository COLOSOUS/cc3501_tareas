# coding=utf-8
"""
Daniel Calderon, CC3501, 2019-1
Drawing 3D cars via scene graph
"""

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys

import transformations2 as tr2
import basic_shapes as bs
import scene_graph2 as sg
import easy_shaders as es
import time
import random

#from crontab import CronTab
#from objloader import *
#cron = CronTab(user=True)


# A class to store the application control
# Add follow_car option
class Controller:
    def __init__(self):
        self.bala1 = True
        self.bala2 = True
        self.showAxis = False
        self.follow_car = False
        self.lights = False
        self.x = 0.0
        self.y = 0.0
        self.ClickOn = False
        #self.theta = 0.0
        self.tabPosx = 0.0
        self.tabPosy = 0.0
"""
def cursor_pos_callback(window, x, y):
    global controller
    controller.mousePos = (x,y)
def mouse_button_callback(window, button, action, mods):

    global controller

  
   
    if (action == glfw.PRESS or action == glfw.REPEAT):
        if (button == glfw.MOUSE_BUTTON_1):
            controller.leftClickOn = True


        if (button == glfw.MOUSE_BUTTON_2):
            None




    elif (action ==glfw.RELEASE):
        if (button == glfw.MOUSE_BUTTON_1):
            controller.leftClickOn = False

        

"""

# we will use the global controller as communication with the callback function
controller = Controller()


def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_SPACE:
        controller.bala1 = not controller.bala1

    elif key == glfw.KEY_LEFT_CONTROL:
        controller.bala2 = not controller.bala2

    #elif key == glfw.KEY_L:
     #   controller.lights = not controller.lights

    elif key == glfw.KEY_LEFT:
        controller.x -= 1.65

    elif key == glfw.KEY_RIGHT:
        controller.x += 1.65

    elif key == glfw.KEY_UP:
        controller.y += 1.37

    elif key == glfw.KEY_DOWN:
        controller.y -= 1.37

    elif key == glfw.KEY_ESCAPE:
        sys.exit()

    elif key == glfw.KEY_C:
        controller.follow_car = not controller.follow_car

    else:
        print('Unknown key')

# Create car depending of isNormal value
"""
def createWarrior():
    #obj=objloader()
    obj=OBJ("warrior.obj")
    wa = sg.SceneGraphNode("wa")
    wa.transform = tr2.scale(1, 1, 1)
    wa.childs += [obj]
    return wa

"""




def createCar(r1,g1,b1, r2, g2, b2, isNormal,N):
    gpuBlackQuad = es.toGPUShape(bs.createColorCube(0,0,0))
    gpuBlackQuad2 = es.toGPUShape(bs.createColorCube(0.4, 0.4, 0.4))
    gpuChasisQuad_color1 = es.toGPUShape(bs.createColorCube(r1,g1,b1))
    gpuChasisQuad_color2 = es.toGPUShape(bs.createColorCube(r2,g2,b2))
    gpuChasisPrism = es.toGPUShape(bs.createColorTriangularPrism(153/255, 204/255, 255/255))
    
    # Cheating a single wheel
    wheel = sg.SceneGraphNode("wheel")
    wheel.transform = tr2.scale(0.2, 0.8, 0.2)
    wheel.childs += [gpuBlackQuad]

    wheelRotation = sg.SceneGraphNode("wheelRotation")
    wheelRotation.childs += [wheel]

    # Instanciating 2 wheels, for the front and back parts
    frontWheel = sg.SceneGraphNode("frontWheel")
    frontWheel.transform = tr2.translate(0.3,0,-0.3)
    frontWheel.childs += [wheelRotation]

    backWheel = sg.SceneGraphNode("backWheel")
    backWheel.transform = tr2.translate(-0.3,0,-0.3)
    backWheel.childs += [wheelRotation]
    
    # Creating the bottom chasis of the car
    if isNormal:

        frontWheel = sg.SceneGraphNode("frontWheel")
        frontWheel.transform = tr2.translate(0.5, 0, -0.3)
        frontWheel.transform = np.matmul(frontWheel.transform , tr2.scale(0.3, 1.1, 0.05))

        frontWheel.childs += [gpuBlackQuad2]

        backWheel = sg.SceneGraphNode("backWheel")
        backWheel.transform = tr2.translate(-0.5, 0, -0.3)
        backWheel.transform = np.matmul(backWheel.transform , tr2.scale(0.3, 1.1, 0.05))
        backWheel.childs += [gpuBlackQuad2]

        frontWhee3 = sg.SceneGraphNode("backWheel3")
        frontWhee3.transform = tr2.translate(0, 0, 2)
        frontWhee3.transform =np.matmul(backWheel.transform,tr2.scale(3, 1, 1))
        frontWhee3.transform = np.matmul(backWheel.transform, tr2.rotationZ(np.pi/2))
        frontWhee3.childs += [gpuBlackQuad]


        bot_chasis = sg.SceneGraphNode("bot_chasis")
        bot_chasis.transform = tr2.scale(1.5, 0.3, 0.5)
        bot_chasis.transform= np.matmul(bot_chasis.transform, tr2.translate(-0.3, 0, 0.15))
        bot_chasis.childs += [gpuChasisQuad_color1]

    else:
        frontWheel = sg.SceneGraphNode("frontWheel")
        frontWheel.transform = tr2.translate(0.3, 0, -0.3)
        frontWheel.childs += [wheelRotation]

        backWheel = sg.SceneGraphNode("backWheel")
        backWheel.transform = tr2.translate(-0.3, 0, -0.3)
        backWheel.childs += [wheelRotation]

        bot_chasis = sg.SceneGraphNode("bot_chasis")
        bot_chasis.transform = tr2.scale(1.1, 0.7, 0.1)
        bot_chasis.childs += [gpuChasisQuad_color1]

    # Moving bottom chasis
    moved_b_chasis = sg.SceneGraphNode("moved_b_chasis")
    moved_b_chasis.transform = tr2.translate(0, 0, -0.2)
    moved_b_chasis.childs += [bot_chasis]

    # Creating light support
    light_s = sg.SceneGraphNode("light_s")
    light_s.transform = tr2.scale(1, 0.1, 0.1)
    light_s.childs += [gpuChasisQuad_color2]

    # Creating right light
    right_light = sg.SceneGraphNode("right_light")
    right_light.transform = tr2.translate(0, 0.25, 0)
    right_light.childs += [light_s]

    # Moving right light
    left_light = sg.SceneGraphNode("left_light")
    left_light.transform = tr2.translate(0, -0.25, 0)
    left_light.childs += [light_s]

    # Creating center chasis
    center_chasis = sg.SceneGraphNode("center_chasis")
    center_chasis.transform = tr2.scale(1, 0.4, 0.15)
    center_chasis.childs += [gpuChasisQuad_color1]

    # Moving center chasis
    m_center_chasis = sg.SceneGraphNode("m_center_chasis")
    m_center_chasis.transform = tr2.translate(0.05, 0, 0)
    m_center_chasis.childs += [center_chasis]

    # Creating center quad
    center_quad = sg.SceneGraphNode("center_quad")
    center_quad.transform = tr2.scale(0.26, 0.5, 0.2)
    center_quad.childs += [gpuChasisQuad_color2]

    # Moving center quad
    m_center_quad = sg.SceneGraphNode("m_center_quad")
    m_center_quad.transform = tr2.translate(-0.07, 0, 0.1)
    m_center_quad.childs += [center_quad]

    # Creating front wind shield
    f_wind_shield = sg.SceneGraphNode("f_wind_shield")
    f_wind_shield.transform = tr2.scale(0.25, 0.5, 0.2)
    f_wind_shield.childs += [gpuChasisPrism]

    # Moving front wind shield
    m_f_wind_shield = sg.SceneGraphNode("m_f_wind_shield")
    m_f_wind_shield.transform = tr2.translate(0.2, 0, 0.1)
    m_f_wind_shield.childs += [f_wind_shield]

    # Creating back wind shield
    b_wind_shield = sg.SceneGraphNode("b_wind_shield")
    b_wind_shield.transform = tr2.scale(0.25, 0.5, 0.2)
    b_wind_shield.childs += [gpuChasisPrism]

    # Rotate back wind shield
    r_b_wind_shield = sg.SceneGraphNode("r_b_wind_shield")
    r_b_wind_shield.transform = tr2.rotationZ(np.pi)
    r_b_wind_shield.childs += [b_wind_shield]

    # Moving back wind shield
    m_b_wind_shield = sg.SceneGraphNode("m_b_wind_shield")
    m_b_wind_shield.transform = tr2.translate(-0.3, 0, 0.1)
    m_b_wind_shield.childs += [r_b_wind_shield]

    # Joining chasis parts
    complete_chasis = sg.SceneGraphNode("complete_chasis")
    complete_chasis.childs += [moved_b_chasis]
    complete_chasis.childs += [right_light]
    complete_chasis.childs += [left_light]
    complete_chasis.childs += [m_center_chasis]
    complete_chasis.childs += [m_center_quad]
    complete_chasis.childs += [m_b_wind_shield]
    complete_chasis.childs += [m_f_wind_shield]


    # All pieces together
    car = sg.SceneGraphNode("scaledenemy_" + str(N))
    car.childs += [complete_chasis]
    car.childs += [frontWheel]

    car.childs += [backWheel]
    if isNormal:
        car.childs += [frontWhee3]

    return car

# Create ground with textures
def createGround():
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad("ttt.png"), GL_REPEAT, GL_NEAREST)
    ground_scaled = sg.SceneGraphNode("ground_scaled")
    ground_scaled.transform = tr2.scale(10, 10, 10)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x")
    ground_rotated.transform = tr2.rotationX(0)
    ground_rotated.childs += [ground_scaled]

    ground = sg.SceneGraphNode("ground")
    ground.transform = tr2.translate(0, 0, 0)
    ground.childs += [ground_rotated]

    return ground
def base():
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad("oo.jpg"), GL_REPEAT, GL_NEAREST)
    base_scaled = sg.SceneGraphNode("base_scaled")
    base_scaled.transform = tr2.scale(200*1.8, 200*1.5, 2)
    base_scaled.childs += [gpuGround_texture]
    return
def world():
    mundo = sg.SceneGraphNode( "mundo")
    return mundo
def balas(Node):
    nodei = sg.findNode(Node, "mundo")
    balas = sg.SceneGraphNode( "balas")
    nodei.childs += [balas]
    return balas
def enemigos(Node):
    nodei = sg.findNode(Node, "mundo")
    enemigos = sg.SceneGraphNode( "enemigos")
    nodei.childs += [enemigos]
    return enemigos
def new_bala1(N,Node,x,y):

    # Cheating a single


    nodei = sg.findNode(Node,"balas")
    gpuBlackQuad = es.toGPUShape(bs.createColorCube(0.2, 0.2, 0.2))
    bala = sg.SceneGraphNode("bala")
    bala.transform = tr2.scale(0.2, 0.8, 0.2)
    node = sg.SceneGraphNode("scaledbala_" + str(N-1))
    bala.childs += [gpuBlackQuad]


    balaRotation = sg.SceneGraphNode("balaRotation")
    balaRotation.childs += [bala]

    # Instanciating 2 wheels, for the front and back parts
    frontbala = sg.SceneGraphNode("frontbala_" + str(N - 1))
    #frontbala.transform = tr2.translate(0.3, 0, -0.3)
    frontbala.transform = tr2.translate(x, y, 0.5)
    frontbala.posx=x
    frontbala.posy=y
    frontbala.transform = np.matmul(frontbala.transform, tr2.rotationZ(-np.pi / 2))

    frontbala.childs += [balaRotation]
    node.childs += [frontbala]
    nodei.childs += [node]
def new_bala2(N, Node, x, y):
    # Cheating a single
    nodei = sg.findNode(Node, "balas")
    gpuBlackQuad = es.toGPUShape(bs.createColorNormalsCube(1, 0.5,1))
    bala = sg.SceneGraphNode("bala")
    bala.transform = tr2.scale(0.2*10, 0.8, 0.2*10)
    node = sg.SceneGraphNode("scaledbala_" + str(N - 1))
    bala.childs += [gpuBlackQuad]
    balaRotation = sg.SceneGraphNode("balaRotation")
    balaRotation.childs += [bala]
    # Instanciating 2 wheels, for the front and back parts
    frontbala = sg.SceneGraphNode("frontbala_" + str(N))
    #frontbala.transform = tr2.translate(0.3, 0, -0.3)
    frontbala.transform = tr2.translate(x , y , 0.5)
    frontbala.posx = x
    frontbala.posy = y
    frontbala.transform = np.matmul(frontbala.transform, tr2.rotationZ(-np.pi / 2))

    frontbala.childs += [balaRotation]
    node.childs += [frontbala]
    nodei.childs += [node]



# Create image of ricardo
def create_paredes(filename):
    gpuAirport_texture = es.toGPUShape(bs.createTextureQuad(filename), GL_REPEAT, GL_LINEAR)
    ricardo_1_scaled = sg.SceneGraphNode("ricardo_scaled")
    ricardo_1_scaled.transform = tr2.scale(15, 15, 15)
    ricardo_1_scaled.childs += [gpuAirport_texture]

    ricardo_1_rotated = sg.SceneGraphNode("ricardo_rotated1")
    ricardo_1_rotated.transform = np.matmul(tr2.rotationX(np.pi / 2), tr2.rotationY(np.pi / 2))
    ricardo_1_rotated.childs += [ricardo_1_scaled]

    ricardo_1 = sg.SceneGraphNode("ricardo1")
    ricardo_1.transform = tr2.translate(6, 0, 1)
    ricardo_1.childs += [ricardo_1_rotated]

    ricardo_2_rotated = sg.SceneGraphNode("ricardo_rotated2")
    ricardo_2_rotated.transform = np.matmul(tr2.rotationX(np.pi / 2), tr2.rotationY(np.pi *0/ 2))
    ricardo_2_rotated.childs += [ricardo_1_scaled]

    ricardo_2 = sg.SceneGraphNode("ricardo2")
    ricardo_2.transform = tr2.translate(0, 6, 1)
    ricardo_2.childs += [ricardo_2_rotated]

    ricardo_3_rotated = sg.SceneGraphNode("ricardo_rotated3")
    ricardo_3_rotated.transform = np.matmul(tr2.rotationX(np.pi / 2), tr2.rotationY(np.pi / 2))
    ricardo_3_rotated.childs += [ricardo_1_scaled]

    ricardo_3 = sg.SceneGraphNode("ricardo3")
    ricardo_3.transform = tr2.translate(-6, 0, 1)
    ricardo_3.childs += [ricardo_3_rotated]

    ricardo_4_rotated = sg.SceneGraphNode("ricardo_rotated4")
    ricardo_4_rotated.transform = np.matmul(tr2.rotationX(np.pi / 2), tr2.rotationY(np.pi *0/ 2))
    ricardo_4_rotated.childs += [ricardo_1_scaled]

    ricardo_4 = sg.SceneGraphNode("ricardo4")
    ricardo_4.transform = tr2.translate(0, -6, 1)
    ricardo_4.childs += [ricardo_4_rotated]

    paredes = sg.SceneGraphNode("paredes")
    paredes.childs += [ricardo_1]
    paredes.childs += [ricardo_2]
    paredes.childs += [ricardo_3]
    paredes.childs += [ricardo_4]


    return paredes
def newE(N):
    i=random.randint(0,5)
    redCarNode1 = createCar(252 / 255, 246 / 255, 246 / 255, 255 / 255, 153 / 255, 153 / 255, controller.lights, N)
    redCarNode1.transform = np.matmul(tr2.rotationZ(-np.pi * 0 / 4), tr2.translate(-4.2*1, 4.2-(1.65*i), 0.5))
    nodei.childs += [redCarNode1]
    return  redCarNode1
def killenemy(w,nn):

    for i in range(nn+2):
        for j in range(nn+2):
            nodo1 = sg.findNode(w, "frontbala_" + str(i))
            nodo2 = sg.findNode(w, "scaledenemy_" + str(j))
            if (nodo1 != None and nodo2 != None):
                nodep1 = sg.findPosition(w, nodo1.name, nodo1.transform)
                nodep2 = sg.findPosition(w, nodo2.name, nodo2.transform)
                y1=nodo1.posy
                y2=nodep2[1]/2
                #print(y1)
                x1=nodep1[0]-nodep1[1]
                x2=nodep2[0]
                #print(x1)
                xx = abs(x1 - x2)
                yy=abs(y1 - y2)

                if (xx <= 0.5 and yy <= 0.5):
                    print("muere >:c")
                    nodo2.transform = np.matmul(nodo2.transform, tr2.translate(-1000, 0, 0))
                    nodo1.transform = np.matmul(nodo2.transform, tr2.translate(-1000, 0, 0))
                    #nodo2.childs= None
                    #nodo1.childs= None
def game_over():
    gpuGround_texture = es.toGPUShape(bs.createTextureQuad("govv.jpg"), GL_REPEAT, GL_NEAREST)
    ground_scaled = sg.SceneGraphNode("ground_scaled2")
    ground_scaled.transform = tr2.scale(10,10,10)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated2_x")
    ground_rotated.transform = tr2.rotationZ(-np.pi/2)
    ground_rotated.transform = np.matmul(ground_rotated.transform,tr2.rotationX(-np.pi*1.2 ))
    ground_rotated.childs += [ground_scaled]

    ground = sg.SceneGraphNode("ground2")
    ground.transform = tr2.translate(0, 0, 2)
    ground.childs += [ground_rotated]

    return ground






if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 1000
    height = 1000

    window = glfw.create_window(width, height, "JUEGO", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program (pipeline) with shaders (simple, texture and lights)
    mvcPipeline = es.SimpleModelViewProjectionShaderProgram()
    textureShaderProgram = es.SimpleTextureModelViewProjectionShaderProgram()
    phongPipeline = es.SimplePhongShaderProgram()




    # Setting up the clear screen color
    glClearColor(1, 1, 1, 1.0)

    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)

    # Creating shapes on GPU memory
    mundo = world()

    enemigos = enemigos(mundo)
    gpuAxis = es.toGPUShape(bs.createAxis(7))
    nodei = sg.findNode(mundo, "enemigos")

    #N2=6

    redCarNode1=newE(0)



    """
    for i in range(N2):
        simpleAI([5, 1], [[0, 0], [0, 1]])
        simpleAI([5, 1], [[0, 0], [0, 1]])
        simpleAI([5, 1], [[0, 0], [0, 1]])
        simpleAI(player, units)
    """

    """
    redCarNode1 = createCar(252/255,246/255,246/255, 255/255, 153/255, 153/255, controller.lights)
    redCarNode2= createCar(252 / 255, 246 / 255, 246 / 255, 255 / 255, 153 / 255, 153 / 255, controller.lights,1)
    redCarNode3 = createCar(252 / 255, 246 / 255, 246 / 255, 255 / 255, 153 / 255, 153 / 255, controller.lights,2)
    redCarNode4 = createCar(252 / 255, 246 / 255, 246 / 255, 255 / 255, 153 / 255, 153 / 255, controller.lights,3)
    redCarNode5 = createCar(252 / 255, 246 / 255, 246 / 255, 255 / 255, 153 / 255, 153 / 255, controller.lights,4)
    redCarNode6 = createCar(252 / 255, 246 / 255, 246 / 255, 255 / 255, 153 / 255, 153 / 255, controller.lights,5)
    
    
    redCarNode1.transform = np.matmul(tr2.rotationZ(-np.pi*0/4), tr2.translate(-4.2,4.2,0.5))
    redCarNode2.transform = np.matmul(tr2.rotationZ(-np.pi*0/4), tr2.translate(-4.2,4.2-a,0.5))
    redCarNode3.transform = np.matmul(tr2.rotationZ(-np.pi*0/4), tr2.translate(-4.2,4.2-2*a,0.5))
    redCarNode4.transform = np.matmul(tr2.rotationZ(-np.pi*0/4), tr2.translate(-4.2,4.2-3*a,0.5))
    redCarNode5.transform = np.matmul(tr2.rotationZ(-np.pi*0/4), tr2.translate(-4.2,4.2-4*a,0.5))
    redCarNode6.transform = np.matmul(tr2.rotationZ(-np.pi*0/4), tr2.translate(-4.2,4.2-5*a,0.5))
    """
    
    blueCarNode = createCar(74/255,111/255,34/255,198/255, 206/255, 0/255, True,0)
    groundNode = createGround()



    base=base()
    ricardoNode = create_paredes("wall.jpg")
    a=1.65
    b=1.37




    blueCarNode.transform = np.matmul(tr2.rotationZ(-np.pi*0/4), tr2.translate(4.2,4.2,0.5))


    #blueCarNode.transform = np.matmul(tr2.rotationZ(-np.pi * 0 / 4), tr2.translate(controller.x, controller.y, 0.0))

    # Define radius of the circumference
    r = 0.1

    # lookAt of normal camera
    normal_view = tr2.lookAt(
            np.array([5, 5, 6]),
            np.array([0, 0, 0]),
            np.array([0, 0, 1])
        )
    d = 0
    N = 0
    balas=balas(mundo)
    t=0


    #new_bala1(N, balas, -controller.y+4.2*10,controller.x-4.2*10)









    while not glfw.window_should_close(window):



        # Telling OpenGL to use our shader program
        glUseProgram(mvcPipeline.shaderProgram)
        # Using the same view and projection matrices in the whole application
        projection = tr2.perspective(60, float(width) / float(height), 0.1, 100)
        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)

        # Calculate coordinates of the camera and redCar
        #u_px = np.cos(glfw.get_time())
        #u_py = np.sin(glfw.get_time())
        #x = r * u_px
        #y = r * u_py

        #u_tx = -u_py
        #u_ty = u_px

        if controller.follow_car:
            """
            # moving camera
            normal_view = tr2.lookAt(
                np.array([x, y, 1]),
                np.array([x + r * u_tx, y + r * u_ty, 1]),
                np.array([0, 0, 1])
            )
            """
        else:
            # static camera
            normal_view = tr2.lookAt(
            np.array([5, 0, 10]),
            np.array([0, 0, 0]),
            np.array([0, 0, 1])
            )

        glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "view"), 1, GL_TRUE, normal_view)

        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        """
        nodePos = sg.findNode(balas, "frontbala_1")
        if (nodePos!=None):
            xy = sg.findPosition(balas, "frontbala_1", nodePos.transform)
            print(xy)
        """



        glUseProgram(mvcPipeline.shaderProgram)
        for i in range(0,N):

            nodeMove = sg.findNode(balas,  "frontbala_"+str(i))
            if nodeMove!=None:
                xy = sg.findPosition(balas, "frontbala_" + str(i), nodeMove.transform)

                nodeMove.transform = np.matmul(nodeMove.transform, tr2.translate(0, -0.05 * 4, 0))
                RotationNode = sg.findNode(nodeMove, "balaRotation")
                RotationNode.transform = np.matmul(RotationNode.transform, tr2.rotationY(0.3 * np.pi / 2))



        killenemy(mundo, N+5)



        if not(controller.bala1):
            N +=1
            #nodePos = sg.findNode(blueCarNode, "car")
            #xy = sg.findPosition(blueCarNode, "car", nodePos.transform)
            #xy=(,)
            new_bala1(N,balas,-controller.y+4.2,controller.x-4.2)

            #print(N)
            controller.bala1=True
        """
        if not(controller.bala2):
            N +=1
            #nodePos = sg.findNode(blueCarNode, "car")
            #xy = sg.findPosition(blueCarNode, "car", nodePos.transform)
            #xy=(,)
            new_bala2(N,balas,-controller.y+4.2,controller.x-4.2)

            #print(N)
            controller.bala2=True
            redCarNode1 = newE(0)
            """
        #controller.bala2=printit
        """
        if not(controller.bala2):
            N +=1

            for i in range(N):
                r1 = -random.randint(0, 1)
                r2 = -random.randint(0, 1)
                enemigo = sg.findNode(enemigos,"scaledenemy_"+str(i))
                if enemigo!=None:
                    enemigo.transform = np.matmul(enemigo.transform, tr2.translate(1.65, r1*r2*1.37 ,0))


            redCarNode1 = newE(N)
            controller.bala2=True
        """
        #print(round(glfw.get_time()))

        umbral=50
        t=t+1
        #print(t)
        if t>umbral:
            N +=1
            t=0

            for i in range(N):
                r1 = -random.randint(0, 1)
                r2 = -random.randint(0, 1)
                enemigo = sg.findNode(enemigos,"scaledenemy_"+str(i))
                if enemigo!=None:
                    if abs(enemigo.transform[1, 3]) < 4:
                        enemigo.transform = np.matmul(enemigo.transform, tr2.translate(1.37, r1*r2*1.65 ,0))
                    else:
                        enemigo.transform = np.matmul(enemigo.transform, tr2.translate(1.37, 0, 0))

                    if enemigo.transform[0,3]>6.5:
                        gov=game_over()

                        glUseProgram(textureShaderProgram.shaderProgram)
                        sg.drawSceneGraphNode(gov, textureShaderProgram)
                        glfw.swap_buffers(window)
                        time.sleep(2)
                        sys.exit()
            redCarNode1 = newE(N)


        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        #else:
            #glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        if controller.showAxis:
            glUniformMatrix4fv(glGetUniformLocation(mvcPipeline.shaderProgram, "model"), 1, GL_TRUE, tr2.identity())
            mvcPipeline.drawShape(gpuAxis, GL_LINES)

        # Moving the red car and rotating its wheels
        #redCarNode.transform = np.matmul(tr2.translate(0, 0, 0.5), tr2.translate(x, y, 0))

        #redCarNode.transform = np.matmul(redCarNode.transform, tr2.rotationZ(glfw.get_time() + np.pi / 2))
        #redWheelRotationNode = sg.findNode(redCarNode, "wheelRotation")
        #redWheelRotationNode.transform = tr2.rotationY(10 * glfw.get_time())

        blueCarNode.transform = np.matmul(tr2.rotationZ(-np.pi * 0 / 4),tr2.translate(4.2 - controller.y, -4.2 + controller.x, 0.5))
        """

        rotar = sg.findNode(balas, "balaRotation")
        rotar.transform = np.matmul(rotar.transform, tr2.rotationZ(10 * glfw.get_time()))
        """

        #rotar = sg.findNode(balas, "balaRotation")
        #rotar.transform =  np.matmul( rotar.transform,tr2.rotationY(5*glfw.get_time()))
        #RotationNode = sg.findNode(balas, "balaRotation")
        #RotationNode.transform =np.matmul(RotationNode.transform,tr2.rotationZ(10*glfw.get_time()))

        controller.tabPosx=4.2 - controller.y
        controller.tabPosy= -4.2 + controller.x






        # Uncomment to print the red car position on every iteration
        #print(sg.findPosition(redCarNode, "car"))

        # Drawing the Car
        sg.drawSceneGraphNode(blueCarNode, mvcPipeline)
        #sg.drawSceneGraphNode(balas, mvcPipeline)
        #balar = sg.findNode(balas, "balaRotation")
        #balar.transform = tr2.rotationY(10 * glfw.get_time())

        #sg.drawSceneGraphNode(warrior, mvcPipeline)
        if not controller.lights:

            sg.drawSceneGraphNode(mundo, mvcPipeline)
            """
            sg.drawSceneGraphNode(redCarNode2, mvcPipeline)
            sg.drawSceneGraphNode(redCarNode3, mvcPipeline)
            sg.drawSceneGraphNode(redCarNode4, mvcPipeline)
            sg.drawSceneGraphNode(redCarNode5, mvcPipeline)
            sg.drawSceneGraphNode(redCarNode6, mvcPipeline)
            """



        # Drawing ground and ricardo using texture shader
        glUseProgram(textureShaderProgram.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(textureShaderProgram.shaderProgram, "view"), 1, GL_TRUE, normal_view)
        # Drawing ground
        sg.drawSceneGraphNode(groundNode, textureShaderProgram)
        sg.drawSceneGraphNode(ricardoNode, textureShaderProgram)
        #sg.drawSceneGraphNode(base, textureShaderProgram)
        #sg.drawSceneGraphNode(gov, textureShaderProgram)


        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    glfw.terminate()