##Hola buenas quisiera explicar el uso de mi c�digo, ya que tuve muchos problemas a la hora de implementarlo,
 y quede sin el tiempo para hacerlo debido a unos controles en otros ramos controles por lo que resumir� lo que entregue:##

El input de la imagen se hace de la siguiente manera:

->En la carpeta debe colocarse la imagen a reparar

->Luego se debe colocar el nombre de la imagen en la siguiente l�nea del c�digo
 im = cv2.imread("arreglar.png")


->En el c�digo se plotea un imshow() de la imagen en el canal rojo(sin reparar).

->Cerrando dicha ventana el c�digo sigue trabajando y hace el m�todo de diferencias finitas.

->Luego de un tiempo considerable se reconstruye la imagen correspondiente al canal rojo y se plotea.


Consideraciones:
Debido a varios factores ya sea el tiempo, complejidad y recursos en software utilizado para hacer e implementar el c�digo  
decid� entregar esta versi�n simplificada, la cual solo aplica el m�todo en el canal ROJO, dado esto el desarrollo es 
an�logo para los otros canales(VERDE,AZUL), para lo cual basta con entregar una matriz concatenando dichos canales y guardar
la imagen resultante.

Agradezco much�simo su comprensi�n.

Pd: hice esta secci�n ya que en la tarea anterior no mencione en ning�n lugar como correr el c�digo lo cual fue un gran error.

