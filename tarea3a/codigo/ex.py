# coding=utf-8
"""
Daniel Calderon, CC3501, 2019-1
Finite Differences for Partial Differential Equations

Solving the Laplace equation in 2D with Dirichlet and
Neumann border conditions over a square domain.
"""

import numpy as np
import matplotlib.pyplot as mpl
import cv2
im = cv2.imread("arreglar.png")
print(im)


s=np.shape(im)
print(s)
c=np.zeros((s[0],s[1]))
s0=np.shape(c)


r=im[:,:,0]
mpl.imshow(r,cmap='gray',interpolation='nearest')
mpl.show()


g=im[:,:,1]
b=im[:,:,2]

s2=np.shape(r)
s3=np.shape(g)
s4=np.shape(b)
print(s0)
print(s2)
print(s3)
print(s4)

def up(r,t):
    s = np.shape(r)
    for i in range(0, s[0]-1):
        for j in range(0, s[1]-1):
            if (j == s[1]):
                break


            top = r[i][j]
            top1 = r[i + 1][j]
            top11 = r[i][j+1]



            if (top == 255 and top1 == 255 and top11 == 255):
                return i-t
        if (i == s[0]):
            break
def down(r,t):
    s = np.shape(r)
    for i in reversed(range(0, s[0]-1)):
        for j in range(0, s[1]-1):
            if (j == s[1]):
                break
            top = r[i][j]
            top1 = r[i-1][j]
            top11 = r[i][j-1]
            if (top == 255 and top1 == 255 and top11 == 255):
                return i+t
        if (i == s[0]):
            break
def left(r,t):
    s = np.shape(r)
    for j in range(0, s[1]-1):
        for i in range(0, s[0]-1):
            if (j == s[0]):
                break
            top = r[i][j]
            top1 = r[i-1][j]
            top11 = r[i][j+1]
            if (top == 255 and top1 == 255 and top11 == 255):
                return j-t
        if (i == s[1]):
            break
def right(r,t):
    s = np.shape(r)
    for j in reversed(range(0, s[1]-1)):
        for i in range(0, s[0]-1):
            if (j == s[0]):
                break
            top = r[i][j]
            top1 = r[i+1][j]
            top11 = r[i][j-1]
            if (top == 255 and top1 == 255 and top11 == 255):
                return j+t
        if (i == s[1]):
            break





uu=up(r,5)
dd=down(r,5)
ll = left(r,5)
rr = right(r,5)


print(uu)
print(dd)
print(rr)
print(ll)
r_r=r[uu:dd,ll:rr]



#mpl.figure()
#mpl.subplot(131)
#mpl.imshow(r_r,cmap='gray',interpolation='nearest')

#mpl.subplot(132)
#mpl.imshow(g,cmap='gray',interpolation='nearest')
#mpl.subplot(133)
#mpl.imshow(b,cmap='gray',interpolation='nearest')
#mpl.show()





# Problem setup
ss=np.shape(r_r)

#mpl.spy(r_r-255)
#mpl.show()

# Boundary Dirichlet Conditions:
TOP = uu
BOTTOM = dd
LEFT = ll
RIGHT = rr

n=0
umbral=180




for ii in range(0,ss[0]):
    for jj in range(0,ss[1]):
        #print(ii)
        #print(jj)
        if (r_r[ii,jj]>=umbral):
            n=n+1
print(n)

#n=(dd-uu)*(rr-ll)
# In this matrix we will write all the coefficients of the unknowns
A = np.zeros((n,n))

# In this vector we will write all the right side of the equations
b = np.zeros((n,))
ci=0
cj=0
def getK(i,j):
    nn = 0
    for ii in range(0, i):
        for jj in range(0, j):
            if (r_r[ii, jj] >= umbral):
                nn = nn + 1
    return  nn
for i in range(0,ss[0]):
    for j in range(0,ss[1]):
#for i in range(dd, uu):
 #   for j in range(ll, rr):

        if (r_r[i,j]>=umbral):


            k = ci


            k_up = getK(i, j + 1)
            k_down = getK(i, j - 1)
            k_left = getK(i - 1, j)
            k_right = getK(i + 1, j)
            #print(k)
            #print(k_up)
            if (r_r[i+1,j]>=umbral):
                A[k, k_up] = 1
                b1=0

            else:
                b1=r_r[i+1,j]

            if (r_r[i-1, j] >=umbral):
                A[k, k_down] = 1
                b2 = 0

            else:
                b2 = r_r[i-1, j]

            if (r_r[i, j-1] >=umbral):
                A[k, k_left] = 1
                b3 = 0

            else:

                b3 = r_r[i, j-1]

            if (r_r[i, j+1] >=umbral):
                A[k, k_right] = 1
                b4 = 0

            else:
                b4 = r_r[i, j+1]
            A[k,k]=-4
            b[ci] =-(b1+b2+b3+b4)
            ci = ci + 1
            cj = cj + 1

print(A)

# Note:
# imshow is also valid but it uses another coordinate system,
# a data transformation is required
#ax.imshow(ub.T)
#mpl.show()
#mpl.spy(A)
#mpl.show()

x = np.linalg.solve(A, b)
print(x)

#mpl.imshow(x,cmap='gray',interpolation='nearest')
#mpl.show()
for i in range(0,ss[0]):
    for j in range(0,ss[1]):
#for i in range(dd, uu):
 #   for j in range(ll, rr):

        if (r_r[i,j]>=umbral):
            r_r[i,j]=x[0]
            #print(r_r[i,j])
            x=np.delete(x,[0])
r[uu:dd,ll:rr]=r_r


imagen_final=r
mpl.imshow(imagen_final,cmap='gray',interpolation='nearest')
mpl.show()





